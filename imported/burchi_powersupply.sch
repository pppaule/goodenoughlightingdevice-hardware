EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power_modules:polulu_2808_switchboard B1
U 1 1 5F1DF81D
P 4800 4850
F 0 "B1" H 5275 4973 50  0000 C CNN
F 1 "polulu_2808_switchboard" H 5275 4880 50  0000 C CNN
F 2 "power_modules:polulu_2808_switchboard" H 4800 4850 50  0001 C CNN
F 3 "" H 4800 4850 50  0001 C CNN
	1    4800 4850
	-1   0    0    1   
$EndComp
$Comp
L power_modules:AdafruitPowerboost1000X B3
U 1 1 5F1E1037
P 5900 2400
F 0 "B3" H 6225 2623 50  0000 C CNN
F 1 "AdafruitPowerboost1000X" H 6225 2530 50  0000 C CNN
F 2 "power_modules:powerboost_1000c" H 5900 2400 50  0001 C CNN
F 3 "" H 5900 2400 50  0001 C CNN
	1    5900 2400
	1    0    0    -1  
$EndComp
$Comp
L power_modules:sparkfun_fuelgauge B2
U 1 1 5F1E1D57
P 4200 1850
F 0 "B2" H 4750 1177 50  0000 C CNN
F 1 "sparkfun_fuelgauge" H 4750 1270 50  0000 C CNN
F 2 "power_modules:sparkfun_fuelgauge" H 4200 1850 50  0001 C CNN
F 3 "" H 4200 1850 50  0001 C CNN
	1    4200 1850
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5F1E2D06
P 1550 2600
F 0 "J1" H 1660 2788 50  0000 C CNN
F 1 "Conn_01x02_Male" H 1660 2695 50  0000 C CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 1550 2600 50  0001 C CNN
F 3 "~" H 1550 2600 50  0001 C CNN
	1    1550 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x06_Male J3
U 1 1 5F1E3F6C
P 1600 4050
F 0 "J3" H 1710 4438 50  0000 C CNN
F 1 "Conn_01x06_Male" H 1710 4345 50  0000 C CNN
F 2 "Connector_JST:JST_PH_B6B-PH-K_1x06_P2.00mm_Vertical" H 1600 4050 50  0001 C CNN
F 3 "~" H 1600 4050 50  0001 C CNN
	1    1600 4050
	1    0    0    -1  
$EndComp
Text Label 2100 3950 0    50   ~ 0
SDA
Text Label 2100 4050 0    50   ~ 0
SCL
Text Label 2100 4150 0    50   ~ 0
5V_IN
Text Label 2100 4250 0    50   ~ 0
5V_OUT
Text Label 2100 4350 0    50   ~ 0
GND
Text Label 1950 2600 0    50   ~ 0
BATT_+
Text Label 1950 2700 0    50   ~ 0
BATT_-
Wire Wire Line
	1750 2600 3600 2600
Wire Wire Line
	3600 2600 3600 2550
Wire Wire Line
	1750 2700 2600 2700
Wire Wire Line
	3700 2700 3700 2250
$Comp
L power:GND #PWR0101
U 1 1 5F1EF3FA
P 2600 3000
F 0 "#PWR0101" H 2600 2750 50  0001 C CNN
F 1 "GND" H 2604 2821 50  0000 C CNN
F 2 "" H 2600 3000 50  0001 C CNN
F 3 "" H 2600 3000 50  0001 C CNN
	1    2600 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5F1F05B1
P 2400 5000
F 0 "#PWR0102" H 2400 4750 50  0001 C CNN
F 1 "GND" H 2404 4821 50  0000 C CNN
F 2 "" H 2400 5000 50  0001 C CNN
F 3 "" H 2400 5000 50  0001 C CNN
	1    2400 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 2700 2600 2800
Connection ~ 2600 2700
Wire Wire Line
	2600 2700 3700 2700
Wire Wire Line
	3100 1700 2900 1700
Wire Wire Line
	2900 1700 2900 3950
Wire Wire Line
	2900 3950 1800 3950
Wire Wire Line
	3100 1600 2800 1600
Wire Wire Line
	2800 1600 2800 4050
Wire Wire Line
	2800 4050 1800 4050
Wire Wire Line
	4200 1600 4550 1600
Wire Wire Line
	4550 1600 4550 2700
Wire Wire Line
	4550 2700 3700 2700
Connection ~ 3700 2700
$Comp
L power:GND #PWR0103
U 1 1 5F1F2C41
P 5450 3400
F 0 "#PWR0103" H 5450 3150 50  0001 C CNN
F 1 "GND" H 5454 3221 50  0000 C CNN
F 2 "" H 5450 3400 50  0001 C CNN
F 3 "" H 5450 3400 50  0001 C CNN
	1    5450 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5F1F31EC
P 7000 4000
F 0 "#PWR0104" H 7000 3750 50  0001 C CNN
F 1 "GND" H 7004 3821 50  0000 C CNN
F 2 "" H 7000 4000 50  0001 C CNN
F 3 "" H 7000 4000 50  0001 C CNN
	1    7000 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 4350 3450 4350
Wire Wire Line
	2400 4350 2400 5000
Wire Wire Line
	2400 4350 1800 4350
Connection ~ 2400 4350
Wire Wire Line
	3700 4450 3450 4450
Wire Wire Line
	3450 4450 3450 4350
Connection ~ 3450 4350
Wire Wire Line
	3450 4350 2400 4350
$Comp
L power:GND #PWR0105
U 1 1 5F1F4C73
P 5800 5050
F 0 "#PWR0105" H 5800 4800 50  0001 C CNN
F 1 "GND" H 5804 4871 50  0000 C CNN
F 2 "" H 5800 5050 50  0001 C CNN
F 3 "" H 5800 5050 50  0001 C CNN
	1    5800 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 4350 5150 4350
Wire Wire Line
	5800 4350 5800 5050
Wire Wire Line
	4950 4450 5150 4450
Wire Wire Line
	5150 4450 5150 4350
Connection ~ 5150 4350
Wire Wire Line
	5150 4350 5800 4350
Wire Wire Line
	3700 4600 2950 4600
Wire Wire Line
	2950 4600 2950 4250
Wire Wire Line
	2950 4250 1800 4250
Wire Wire Line
	5100 4600 4950 4600
Wire Wire Line
	4950 4700 5000 4700
Wire Wire Line
	5100 4700 5100 4600
Wire Wire Line
	3700 4700 3450 4700
Wire Wire Line
	2950 4700 2950 4600
Connection ~ 2950 4600
Wire Wire Line
	5100 4700 5100 5400
Wire Wire Line
	5100 5400 7550 5400
Wire Wire Line
	7550 5400 7550 2450
Wire Wire Line
	7550 2450 6800 2450
Connection ~ 5100 4700
Wire Wire Line
	6800 3000 7000 3000
Wire Wire Line
	7000 3000 7000 4000
Wire Wire Line
	5650 3000 5450 3000
Wire Wire Line
	5450 3000 5450 3400
Wire Wire Line
	3250 4150 3250 3550
Wire Wire Line
	3250 3550 5150 3550
Wire Wire Line
	5150 3550 5150 2450
Wire Wire Line
	5150 2450 5650 2450
Wire Wire Line
	1800 4150 3250 4150
Wire Wire Line
	5650 2550 3600 2550
Connection ~ 3600 2550
Wire Wire Line
	3600 2550 3600 2250
$Comp
L Device:LED D1
U 1 1 5F20807E
P 8200 4150
F 0 "D1" V 8240 4027 50  0000 R CNN
F 1 "POWER_LED" H 8147 4027 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 8200 4150 50  0001 C CNN
F 3 "~" H 8200 4150 50  0001 C CNN
	1    8200 4150
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D2
U 1 1 5F20A2D6
P 8550 4150
F 0 "D2" V 8495 4229 50  0000 L CNN
F 1 "CHARGE_LED" H 8588 4229 50  0000 L CNN
F 2 "LED_THT:LED_D5.0mm" H 8550 4150 50  0001 C CNN
F 3 "~" H 8550 4150 50  0001 C CNN
	1    8550 4150
	0    1    1    0   
$EndComp
$Comp
L Device:LED D3
U 1 1 5F20A625
P 8900 4150
F 0 "D3" V 8845 4229 50  0000 L CNN
F 1 "CHRG_DONE_LED" H 8938 4229 50  0000 L CNN
F 2 "LED_THT:LED_D5.0mm" H 8900 4150 50  0001 C CNN
F 3 "~" H 8900 4150 50  0001 C CNN
	1    8900 4150
	0    1    1    0   
$EndComp
$Comp
L Device:LED D4
U 1 1 5F20A9CB
P 9250 4150
F 0 "D4" V 9290 4028 50  0000 R CNN
F 1 "LOBATT_LED" H 9197 4028 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 9250 4150 50  0001 C CNN
F 3 "~" H 9250 4150 50  0001 C CNN
	1    9250 4150
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 5F20D961
P 8200 3650
F 0 "R1" H 8270 3697 50  0000 L CNN
F 1 "R" H 8270 3604 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8130 3650 50  0001 C CNN
F 3 "~" H 8200 3650 50  0001 C CNN
	1    8200 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5F20DCA6
P 8550 3650
F 0 "R2" H 8620 3697 50  0000 L CNN
F 1 "R" H 8620 3604 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8480 3650 50  0001 C CNN
F 3 "~" H 8550 3650 50  0001 C CNN
	1    8550 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5F20E144
P 8900 3650
F 0 "R3" H 8970 3697 50  0000 L CNN
F 1 "R" H 8970 3604 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8830 3650 50  0001 C CNN
F 3 "~" H 8900 3650 50  0001 C CNN
	1    8900 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5F20E579
P 9250 3650
F 0 "R4" H 9320 3697 50  0000 L CNN
F 1 "R" H 9320 3604 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9180 3650 50  0001 C CNN
F 3 "~" H 9250 3650 50  0001 C CNN
	1    9250 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 3250 8200 3250
Wire Wire Line
	8200 3250 8200 3500
Wire Wire Line
	8200 3800 8200 4000
Wire Wire Line
	8550 3350 8550 3500
Wire Wire Line
	6800 3350 8550 3350
Wire Wire Line
	8550 3800 8550 4000
Wire Wire Line
	6800 3450 7900 3450
Wire Wire Line
	7900 3450 7900 3150
Wire Wire Line
	7900 3150 8900 3150
Wire Wire Line
	8900 3150 8900 3500
Wire Wire Line
	6800 2750 9250 2750
Wire Wire Line
	9250 2750 9250 3500
Wire Wire Line
	9250 3800 9250 4000
Wire Wire Line
	8900 3800 8900 4000
Wire Wire Line
	8550 4500 8550 4300
Wire Wire Line
	8550 4500 8900 4500
$Comp
L power:GND #PWR0106
U 1 1 5F21A50B
P 8200 4800
F 0 "#PWR0106" H 8200 4550 50  0001 C CNN
F 1 "GND" H 8204 4621 50  0000 C CNN
F 2 "" H 8200 4800 50  0001 C CNN
F 3 "" H 8200 4800 50  0001 C CNN
	1    8200 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 4300 8200 4600
Wire Wire Line
	8900 4300 8900 4500
Wire Wire Line
	8900 4500 9750 4500
Wire Wire Line
	9750 4500 9750 2100
Wire Wire Line
	9750 2100 5150 2100
Wire Wire Line
	5150 2100 5150 2450
Connection ~ 8900 4500
Connection ~ 5150 2450
Wire Wire Line
	9250 4300 9250 4600
Wire Wire Line
	9250 4600 8200 4600
Connection ~ 8200 4600
Wire Wire Line
	8200 4600 8200 4800
$Comp
L Connector:Conn_01x04_Male J2
U 1 1 5F5178EE
P 1600 3150
F 0 "J2" H 1710 3438 50  0000 C CNN
F 1 "Conn_01x04_Male" H 1710 3345 50  0000 C CNN
F 2 "Connector_JST:JST_PH_B4B-PH-K_1x04_P2.00mm_Vertical" H 1600 3150 50  0001 C CNN
F 3 "~" H 1600 3150 50  0001 C CNN
	1    1600 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 3250 3500 3250
Wire Wire Line
	6800 2850 7350 2850
Wire Wire Line
	7350 2850 7350 3700
Wire Wire Line
	7350 3700 2450 3700
Wire Wire Line
	2450 3700 2450 3150
Wire Wire Line
	2450 3150 1800 3150
Wire Wire Line
	1800 3050 2300 3050
Wire Wire Line
	2300 3050 2300 2800
Wire Wire Line
	2300 2800 2600 2800
Connection ~ 2600 2800
Wire Wire Line
	2600 2800 2600 3000
Text Notes 600  3250 0    50   ~ 0
pin 1 + 2: vreg enable\npin 3 + 4: polulu switch
$Comp
L Jumper:Jumper_2_Open JP1
U 1 1 5F70576B
P 4350 5400
F 0 "JP1" H 4350 5643 50  0000 C CNN
F 1 "Jumper_2_Open" H 4350 5550 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 4350 5400 50  0001 C CNN
F 3 "~" H 4350 5400 50  0001 C CNN
	1    4350 5400
	1    0    0    -1  
$EndComp
Text Notes 4100 5600 0    50   ~ 0
BYPASS_JUMPER
Wire Wire Line
	5000 4700 5000 5400
Wire Wire Line
	5000 5400 4550 5400
Connection ~ 5000 4700
Wire Wire Line
	5000 4700 5100 4700
Wire Wire Line
	4150 5400 3450 5400
Wire Wire Line
	3450 5400 3450 4700
Connection ~ 3450 4700
Wire Wire Line
	3450 4700 2950 4700
Wire Wire Line
	1800 3850 2200 3850
Wire Wire Line
	2200 3850 2200 3700
$Comp
L power:+3V3 #PWR0107
U 1 1 5F70FA13
P 2200 3700
F 0 "#PWR0107" H 2200 3550 50  0001 C CNN
F 1 "+3V3" H 2214 3880 50  0000 C CNN
F 2 "" H 2200 3700 50  0001 C CNN
F 3 "" H 2200 3700 50  0001 C CNN
	1    2200 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 4200 3700 4200
Wire Wire Line
	3500 3250 3500 4200
Wire Wire Line
	3700 4100 3600 4100
Wire Wire Line
	3600 4100 3600 3350
Wire Wire Line
	1800 3350 3600 3350
$EndSCHEMATC
